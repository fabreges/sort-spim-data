NAME
        ./main.sh -- Sort Luxendo's SPIM data into a new folder.

SYNOPSYS
        ./main.sh [--force] [--run=(dry|move|copy|link|command)] [--camperchannel=0] --dataset=DATASET SPIMfolder outfolder

DESCRIPTION
        The script reads a folder containing one or multiple SPIM folder, and operate on all files one by one,
        stack by stack, channel by channel and camera by camera. Multiple simple operation are available for
        each file: move, copy or link the files. The destination is given by the 'outfolder' parameter. Two
        other options are available: dry, to simulate the operation without doing anything, and a custom command
        executed in a subshell.

        The SPIM folder must contain one or multiple folders named after the date of creation: yyyy-mm-dd_HHMMSS.
        The folder will be considered as part of the same experiment, and will be read in chronological order.
        Each of these subfolders must contain one or multiple stackchannel folders: stack_n_channel_m. The script
        does not work with tiles yet. Finally, each stackchannel folder must contain a file named with the
        following convention: /^Cam_(Long|Short)_([0-9]{5})(.*)$/. The match \1 gives the camera number, either
        0 (Long) or 1 (Short). The match \2 gives the timpoint of the current stackchannel folder. The match \3
        is the 'rest' and is kept for the final filename.

        The output filename will be named according to the following convention:
        /^(.*)([a-z])_t([0-9]{3,})_ch([0-9]{2,})_cam([0-9]{2,})(.*)$/.
        The first match is the dataset name. The second match is a letter corresponding to the stack number.
        Stack 0 gives 'a', stack 1 gives 'b', and so on. The third match is the cumulative timepoint for the
        current stack. E.g., if stack 0 exists in two 'date and time' folder, the files will start in both folder
        at timepoint 0. The final filename will assume that the imaging never stopped and that the first
        timepoint in the second folder is directly following the last timepoint of the first folder. The fourth
        match corresponds to the channel number. The fifth match to the camera number, and finally the sixth
        match corresponds to the rest of the filename.

        Output files are sorted into subfolders named after the dataset name and the stack letter.

        --force
             Do not stop if the target file already exists.

        --run
             Either dry (print out operation), move (move files), copy (copy files), link (symlink files)
             or any command that can be interpreted by bash. The custom command, if not 'dry', 'move', 
             'copy' or 'link', will be send to bash with the following command: 'bash -c "command"'.
             The source and the destination are stored in the environmental variable $SOURCE and $TARGET.

       --camperchannel:
             If more than 0, the 'ch' and 'cam' parameters will be fused in a single 'ch' parameter.
             The output channel will be computed as the product of the channel and the camperchannel
             value, plus the cam number. Channel 1 with 2 camera per channel gives: 'ch02' and 'ch03'
             instead of 'ch01_cam00' and 'ch01_cam01'.

       --dataset:
             Prefix to add to the dataset. The final name will start with DATASETx_, with 'DATASET' the
             value given by the user, and 'x' a letter corresponding to the stack number (0=a, 1=b, ...)

EXAMPLE
        $ tree
         ./
         ├── Output/
         └── SPIMFolder/
             ├── 2020-01-01_102030/
             │   ├── stack_0_channel_0/
             │   │   ├── Cam_Long_00000.tif
             │   │   ├── Cam_Long_00001.tif
             │   │   └── Cam_Long_00002.tif
             │   ├── stack_0_channel_1/
             │   │   ├── Cam_Long_00000.tif
             │   │   ├── Cam_Long_00001.tif
             │   │   └── Cam_Long_00002.tif
             │   ├── stack_1_channel_0/
             │   │   ├── Cam_Long_00000.tif
             │   │   ├── Cam_Long_00001.tif
             │   │   └── Cam_Long_00002.tif
             │   └── stack_1_channel_1/
             │       ├── Cam_Long_00000.tif
             │       ├── Cam_Long_00001.tif
             │       └── Cam_Long_00002.tif
             └── 2020-01-01_111515/
                 ├── stack_0_channel_0/
                 │   ├── Cam_Long_00000.tif
                 │   ├── Cam_Long_00001.tif
                 │   └── Cam_Long_00002.tif
                 └── stack_0_channel_1/
                     ├── Cam_Long_00000.tif
                     ├── Cam_Long_00001.tif
                     └── Cam_Long_00002.tif

        $ ./main.sh --run=move --dataset=200101 --camperchannel=0 /path/to/SPIMFolder /path/to/Output && tree
         ./
         ├── Output/
         │   ├── 200101a_t000_ch00_cam00.tif
         │   ├── 200101a_t000_ch01_cam00.tif
         │   ├── 200101a_t001_ch00_cam00.tif
         │   ├── 200101a_t001_ch01_cam00.tif
         │   ├── 200101a_t002_ch00_cam00.tif
         │   ├── 200101a_t002_ch01_cam00.tif
         │   ├── 200101a_t003_ch00_cam00.tif
         │   ├── 200101a_t003_ch01_cam00.tif
         │   ├── 200101a_t004_ch00_cam00.tif
         │   ├── 200101a_t004_ch01_cam00.tif
         │   ├── 200101a_t005_ch00_cam00.tif
         │   ├── 200101a_t005_ch01_cam00.tif
         │   ├── 200101b_t000_ch00_cam00.tif
         │   ├── 200101b_t000_ch01_cam00.tif
         │   ├── 200101b_t001_ch00_cam00.tif
         │   ├── 200101b_t001_ch01_cam00.tif
         │   ├── 200101b_t002_ch00_cam00.tif
         │   └── 200101b_t002_ch01_cam00.tif
         └── SPIMFolder/
             ├── 2020-01-01_102030/
             │   ├── stack_0_channel_0/
             │   ├── stack_0_channel_1/
             │   ├── stack_1_channel_0/
             │   └── stack_1_channel_1/
             └── 2020-01-01_111515/
                 ├── stack_0_channel_0/
                 └── stack_0_channel_1/

BUGS
        The script does not work with tiles.

AUTHORS
        Dimitri Fabrèges: dimitri.fabreges@embl.de
