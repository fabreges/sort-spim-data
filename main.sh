#! /bin/bash

dataset=
camperchannel=0
run=dry
force=false

usage()
{
  echo "NAME"
  echo "        $0 -- Sort Luxendo's SPIM data into a new folder."
  echo ""
  echo "SYNOPSYS"
  echo "        $0 [--force] [--run=(dry|move|copy|link|command)] [--camperchannel=0] --dataset=DATASET SPIMfolder outfolder"
  echo ""
  echo "DESCRIPTION"
  echo "        The script reads a folder containing one or multiple SPIM folder, and operate on all files one by one,"
  echo "        stack by stack, channel by channel and camera by camera. Multiple simple operation are available for"
  echo "        each file: move, copy or link the files. The destination is given by the 'outfolder' parameter. Two"
  echo "        other options are available: dry, to simulate the operation without doing anything, and a custom command"
  echo "        executed in a subshell."
  echo ""
  echo "        The SPIM folder must contain one or multiple folders named after the date of creation: yyyy-mm-dd_HHMMSS."
  echo "        The folder will be considered as part of the same experiment, and will be read in chronological order."
  echo "        Each of these subfolders must contain one or multiple stackchannel folders: stack_n_channel_m. The script"
  echo "        does not work with tiles yet. Finally, each stackchannel folder must contain a file named with the"
  echo "        following convention: /^Cam_(Long|Short)_([0-9]{5})(.*)$/. The match \\1 gives the camera number, either"
  echo "        0 (Long) or 1 (Short). The match \\2 gives the timpoint of the current stackchannel folder. The match \\3"
  echo "        is the 'rest' and is kept for the final filename."
  echo ""
  echo "        The output filename will be named according to the following convention:"
  echo "        /^(.*)([a-z])_t([0-9]{3,})_ch([0-9]{2,})_cam([0-9]{2,})(.*)$/."
  echo "        The first match is the dataset name. The second match is a letter corresponding to the stack number."
  echo "        Stack 0 gives 'a', stack 1 gives 'b', and so on. The third match is the cumulative timepoint for the"
  echo "        current stack. E.g., if stack 0 exists in two 'date and time' folder, the files will start in both folder"
  echo "        at timepoint 0. The final filename will assume that the imaging never stopped and that the first"
  echo "        timepoint in the second folder is directly following the last timepoint of the first folder. The fourth"
  echo "        match corresponds to the channel number. The fifth match to the camera number, and finally the sixth"
  echo "        match corresponds to the rest of the filename."
  echo ""
  echo "        Output files are sorted into subfolders named after the dataset name and the stack letter."
  echo ""
  echo "        --force"
  echo "             Do not stop if the target file already exists."
  echo ""
  echo "        --run"
  echo "             Either dry (print out operation), move (move files), copy (copy files), link (symlink files)"
  echo "             or any command that can be interpreted by bash. The custom command, if not 'dry', 'move', "
  echo "             'copy' or 'link', will be send to bash with the following command: 'bash -c \"command\"'."
  echo "             The source and the destination are stored in the environmental variable \$SOURCE and \$TARGET."
  echo ""
  echo "       --camperchannel:"
  echo "             If more than 0, the 'ch' and 'cam' parameters will be fused in a single 'ch' parameter."
  echo "             The output channel will be computed as the product of the channel and the camperchannel"
  echo "             value, plus the cam number. Channel 1 with 2 camera per channel gives: 'ch02' and 'ch03'"
  echo "             instead of 'ch01_cam00' and 'ch01_cam01'."
  echo ""
  echo "       --dataset:"
  echo "             Prefix to add to the dataset. The final name will start with DATASETx_, with 'DATASET' the"
  echo "             value given by the user, and 'x' a letter corresponding to the stack number (0=a, 1=b, ...)"
  echo ""
  echo "EXAMPLE"
  echo "        $ tree"
  echo "         ./"
  echo "         ├── Output/"
  echo "         └── SPIMFolder/"
  echo "             ├── 2020-01-01_102030/"
  echo "             │   ├── stack_0_channel_0/"
  echo "             │   │   ├── Cam_Long_00000.tif"
  echo "             │   │   ├── Cam_Long_00001.tif"
  echo "             │   │   └── Cam_Long_00002.tif"
  echo "             │   ├── stack_0_channel_1/"
  echo "             │   │   ├── Cam_Long_00000.tif"
  echo "             │   │   ├── Cam_Long_00001.tif"
  echo "             │   │   └── Cam_Long_00002.tif"
  echo "             │   ├── stack_1_channel_0/"
  echo "             │   │   ├── Cam_Long_00000.tif"
  echo "             │   │   ├── Cam_Long_00001.tif"
  echo "             │   │   └── Cam_Long_00002.tif"
  echo "             │   └── stack_1_channel_1/"
  echo "             │       ├── Cam_Long_00000.tif"
  echo "             │       ├── Cam_Long_00001.tif"
  echo "             │       └── Cam_Long_00002.tif"
  echo "             └── 2020-01-01_111515/"
  echo "                 ├── stack_0_channel_0/"
  echo "                 │   ├── Cam_Long_00000.tif"
  echo "                 │   ├── Cam_Long_00001.tif"
  echo "                 │   └── Cam_Long_00002.tif"
  echo "                 └── stack_0_channel_1/"
  echo "                     ├── Cam_Long_00000.tif"
  echo "                     ├── Cam_Long_00001.tif"
  echo "                     └── Cam_Long_00002.tif"
  echo ""
  echo "        $ ./main.sh --run=move --dataset=200101 --camperchannel=0 /path/to/SPIMFolder /path/to/Output && tree"
  echo "         ./"
  echo "         ├── Output/"
  echo "         │   ├── 200101a_t000_ch00_cam00.tif"
  echo "         │   ├── 200101a_t000_ch01_cam00.tif"
  echo "         │   ├── 200101a_t001_ch00_cam00.tif"
  echo "         │   ├── 200101a_t001_ch01_cam00.tif"
  echo "         │   ├── 200101a_t002_ch00_cam00.tif"
  echo "         │   ├── 200101a_t002_ch01_cam00.tif"
  echo "         │   ├── 200101a_t003_ch00_cam00.tif"
  echo "         │   ├── 200101a_t003_ch01_cam00.tif"
  echo "         │   ├── 200101a_t004_ch00_cam00.tif"
  echo "         │   ├── 200101a_t004_ch01_cam00.tif"
  echo "         │   ├── 200101a_t005_ch00_cam00.tif"
  echo "         │   ├── 200101a_t005_ch01_cam00.tif"
  echo "         │   ├── 200101b_t000_ch00_cam00.tif"
  echo "         │   ├── 200101b_t000_ch01_cam00.tif"
  echo "         │   ├── 200101b_t001_ch00_cam00.tif"
  echo "         │   ├── 200101b_t001_ch01_cam00.tif"
  echo "         │   ├── 200101b_t002_ch00_cam00.tif"
  echo "         │   └── 200101b_t002_ch01_cam00.tif"
  echo "         └── SPIMFolder/"
  echo "             ├── 2020-01-01_102030/"
  echo "             │   ├── stack_0_channel_0/"
  echo "             │   ├── stack_0_channel_1/"
  echo "             │   ├── stack_1_channel_0/"
  echo "             │   └── stack_1_channel_1/"
  echo "             └── 2020-01-01_111515/"
  echo "                 ├── stack_0_channel_0/"
  echo "                 └── stack_0_channel_1/"
  echo ""
  echo "BUGS"
  echo "        The script does not work with tiles."
  echo ""
  echo "AUTHORS"
  echo "        Dimitri Fabrèges: dimitri.fabreges@embl.de"
}

if [ $# -eq 0 ]
then
  usage
  exit 0
fi

while [ $# -gt 2 ]
do
  left="${1%%=*}"
  right="${1#*=}"

  if [ "${left}" == "--run" ]
  then
    run="${right}"
  elif [ "${left}" == "--dataset" ]
  then
    dataset="${right}"
  elif [ "${left}" == "--force" ]
  then
    force=true
  elif [ "${left}" == "--camperchannel" ]
  then
    camperchannel=$((right))
  fi

  shift
done

root="$1"; shift
outfolder="$1"

tmp=/tmp/$RANDOM$RANDOM$RANDOM
mkdir -p "${tmp}"

# Letters for dataset name
letters=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

# Check whether the root folder is good or not
if [ -z "${root}" -o -z "${outfolder}" -o -z "${dataset}" ]
then
  usage >&2
  exit 1
fi

if [ "${run}" != "dry" ]
then
  mkdir -p "${outfolder}"
fi

if [ ! -d "${root}" ]
then
  usage >&2

  exit 2
fi

# Root folder *must* contains subfolder with the following pattern:
# YYYY-mm-dd_HHMMSS
timefolders=()
for f in "${root}"/*
do
  if [ ! -d "${f}" ]
  then
    continue
  fi

  name="$(basename "${f}")"
  if echo "${name}" | grep -E '^[0-9]{4}(-[0-9]{2}){2}_[0-9]{6}$' &>/dev/null
  then
    timefolders+=("${name}")

    ls -d "${f}"/stack_*_channel_* | awk -F_ '{print $3" "$5}' >> "${tmp}/stackchannels"
  fi
done

if [ ${#timefolders[@]} -eq 0 ]
then
  >&2 echo "No time folder found."
  exit 3
fi

# Sort timefolders
IFS=$'\n' sorted=($(sort <<< "${timefolders[*]}"))
unset IFS

# Get the number of stacks and channels
sort -u "${tmp}/stackchannels" | while read sc
do
  stack="$(echo "${sc}" | cut -d ' ' -f 1)"
  channel="$(echo "${sc}" | cut -d ' ' -f 2)"
  timestep=0
  current=0

  letter=${letters[${stack}]}

  for time in "${sorted[@]}"
  do
    datafolder="${root}/${time}/stack_${stack}_channel_${channel}"
    if [ ! -d "${datafolder}" ]
    then
      continue
    fi

    for filepath in "${datafolder}"/*
    do
      filename="$(basename "${filepath}")"
      corename="${filename%%.*}"
      filecam="$(echo "${corename}" | cut -d '_' -f 1)"
      camtype="$(echo "${corename}" | cut -d '_' -f 2)"
      tmpoint="$(echo "${corename}" | cut -d '_' -f 3)"
      restarg="${filename: $((${#filecam} + ${#camtype} + ${#tmpoint} + 2))}"

      if [ "${filecam}" != "Cam" -o -z "${camtype}" -o -z "${tmpoint}" ]
      then
        echo "${filename} has an unknown pattern. Skipping..."
        continue
      fi

      if echo "${tmpoint}" | grep -vE '^[0-9]+$' &>/dev/null
      then
        echo "${filename} has an unknown pattern. Skipping..."
        continue
      fi

      timepoint=$(echo "${tmpoint}" | sed -E 's/^0+//')
      timepoint=$((timepoint + timestep))
      if [ ${timepoint} -gt ${current} ]
      then
        current=${timepoint}
      fi

      camno=2
      if [ "${camtype}" == "Long"  ]; then camno=0; fi
      if [ "${camtype}" == "Short" ]; then camno=1; fi
      outchannel=$((channel * camperchannel + camno))

      if [ ${camperchannel} -gt 0 ]
      then
        outname="$(printf '%s%s_t%03d_ch%02d%s\n' "${dataset}" "${letter}" ${timepoint} ${outchannel} "${restarg}")"
      else
        outname="$(printf '%s%s_t%03d_ch%02d_cam%02d%s\n' "${dataset}" "${letter}" ${timepoint} ${channel} ${camno} "${restarg}")"
      fi

      datasetfolder="${outfolder}/${dataset}${letter}"
      outpath="${datasetfolder}/${outname}"

      if [ -e "${outpath}" -a "${force}" == "false" ]
      then
        >&2 echo "Output file already exist: quitting."
        exit 3
      fi

      if [ "${run}" == "dry" ]
      then
        echo "${filepath} ---> ${outpath}"
      elif [ "${run}" == "move" ]
      then
        mkdir -p "${datasetfolder}"
        mv -v "${filepath}" "${outpath}"
      elif [ "${run}" == "copy" ]
      then
        mkdir -p "${datasetfolder}"
        cp -v "${filepath}" "${outpath}"
      elif [ "${run}" == "link" ]
      then
        mkdir -p "${datasetfolder}"
        ln -v -s "${filepath}" "${outpath}"
      else
        mkdir -p "${datasetfolder}"
        SOURCE="${filepath}" TARGET="${outpath}" bash -c "${run}"
      fi
    done

    timestep=$((current + 1))
  done
done

rm -r "${tmp}"
